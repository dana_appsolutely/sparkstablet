$(function(){
	addEventListeners();

	syncData();
});

function addEventListeners(){
	$(".campaign-list ul").on("tap",modalShow);	
	$("#redeem").on("tap",login);
	$("#stamp").on("tap",checkIn);
	$("#settings img").on("tap",settings);
	$("#sync").on("tap", function(){
		nativeInterface.syncEarnedPoints();
	})

	//MODAL 
	$("#choice #cancel").on("tap",modalHide);
	$("#warning #ok").on("tap",modalHide);
	$(".reg-modal #cancel").on("tap",modalHide);
	$("#choice #ok").on("tap",redeemItem);
	$("#register").on("tap",register);
	$("#register #register-btn").on("tap",validateRegistration);

	$("#logout").on("tap",logout);
}

function login(){
	nativeInterface.logIn();
}

//LOGIN CALLBACK SHOW REWARDS/CAMPAIGN LIST
function logInCallback(jsonString){
	var rewards = JSON.parse(jsonString);
	$("#home").removeClass('active-view');
	$("#campaign").addClass('active-view');
	
	//TEST
	// var rewards = [{"campaign": [{"loyaltyID": "LOYGE188151DlbRl","name": "Free Coffee","points": "10","frequency": "0","description": "<p>123</p>","terms": "testing","promoType": "snap"},{"loyaltyID": "LOYmr16955GILwhZ","name": "Cornetto 3 + 1","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App.","promoType": "frequency"},{"loyaltyID": "LOY5e151153dc8mf","name": "Aloha Burger","points": "10","frequency": "0","description": "<p>A big sarap burger</p>","terms": "secret","promoType": "snap"},{"loyaltyID": "LOYZE1612386j6yQ","name": "FREE Tumbler and 12oz Nescafe Shake","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "","promoType": "frequency"},{"loyaltyID": "LOYQx161338TE43Y","name": "Sola 2 + 1","points": "2","frequency": "0","description": "<p>Test</p>","terms": "Test","promoType": "snap"}]},{"profile": {"raffleEntry": 0,"totalPoints": 12,"fname": "Bat","mname": "","lname": "Saver","email": "albert.madia@yahoo.com","mobileNum": "","landlineNum": "","dateReg": "2015-03-24","qrApp": "","memberID": "MEMYl121217GQZLC","redeemable_sku_promo": [{"loyaltyID": "LOYmr16955GILwhZ","count": 0,"group": "Selecta Cornetto"},{"loyaltyID": "LOYZE1612386j6yQ","count": 0,"group": "Nescafe Shake"}],"lastSync": "Oct 16,2015"}}]
	
	var profile = rewards[1].profile;
	var campaign = rewards[0].campaign;
	var content = " ";
 
	$('#user-email').html(profile.email);
	$('#totalpoints').html(profile.totalPoints);
	for (var i = 0; i < campaign.length; i++) {
		content+= '<li data-title="'+encodeURIComponent(campaign[i].name)+'" data-points="'+campaign[i].points+'" data-id="'+campaign[i].loyaltyID+'" class="'+((parseInt(profile.totalPoints) >= parseInt(campaign[i].points))? "redeem": "")+'">'+
					'<div class="points"><div data-label="'+((parseInt(campaign[i].points)==1) ? "POINT" : "POINTS")+'">'+campaign[i].points+'</div></div>'+
					'<div class="desc">'+campaign[i].name+'</div>'+
				  '</li>';
				  console.log((parseInt(profile.totalPoints) >= parseInt(campaign[i].points))? "redeem": "");
	};
	$('.campaign-list ul').html(content);
}


function modalShow(e){
	if (e.target != e.currentTarget) {
		var title = e.target.getAttribute('data-title');
		var points = e.target.getAttribute('data-points');
		var id = e.target.getAttribute('data-id');
		
		$('#modal').removeClass().addClass('overlay');
		if($(e.target).hasClass('redeem')){
			$('#choice .msg').html("Are you sure you want to redeem " +decodeURIComponent(title) + "?");
			$('#modal').addClass('choice');	
			$('#choice #ok')[0].setAttribute('data-points', points);
			$('#choice #ok')[0].setAttribute('data-title', title);
			$('#choice #ok')[0].setAttribute('data-id', id);
		}else{
			$('#warning .msg').html("You need more points to redeem " +decodeURIComponent(title));
			$('#modal').addClass('warning');	
		}
	}
	
}

//SYNC DATA
function syncData(){
  var count = parseInt(nativeInterface.getEarnedCount());
  // var count = 10;
    if (!isNaN(count)) {
        $("#count").html(count);
    }else{
        $("#count").html("0");
    }
}

function syncCallback(){
 	syncData();
}

function earnCallback(){
 	syncData();
}

function modalHide(){
	$('#modal').removeClass().addClass('overlay');
	$('.reg-modal').hide();
}

function redeemItem(e){

	// $("#home").removeClass('active-view');
	// $("#campaign").addClass('active-view');
	
	// var rewards = [{"campaign": [{"loyaltyID": "LOYGE188151DlbRl","name": "Free Coffee","points": "10","frequency": "0","description": "<p>123</p>","terms": "testing","promoType": "snap"},{"loyaltyID": "LOYmr16955GILwhZ","name": "Cornetto 3 + 1","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App.","promoType": "frequency"},{"loyaltyID": "LOY5e151153dc8mf","name": "Aloha Burger","points": "10","frequency": "0","description": "<p>A big sarap burger</p>","terms": "secret","promoType": "snap"},{"loyaltyID": "LOYZE1612386j6yQ","name": "FREE Tumbler and 12oz Nescafe Shake","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "","promoType": "frequency"},{"loyaltyID": "LOYQx161338TE43Y","name": "Sola 2 + 1","points": "2","frequency": "0","description": "<p>Test</p>","terms": "Test","promoType": "snap"}]},{"profile": {"raffleEntry": 0,"totalPoints": 1,"fname": "Bat","mname": "","lname": "Saver","email": "albert.madia@yahoo.com","mobileNum": "","landlineNum": "","dateReg": "2015-03-24","qrApp": "","memberID": "MEMYl121217GQZLC","redeemable_sku_promo": [{"loyaltyID": "LOYmr16955GILwhZ","count": 0,"group": "Selecta Cornetto"},{"loyaltyID": "LOYZE1612386j6yQ","count": 0,"group": "Nescafe Shake"}],"lastSync": "Oct 16,2015"}}]
	
	// var profile = rewards[1].profile;
	// var campaign = rewards[0].campaign;
	// var content = " ";

	// $('#user-email').html(profile.email);
	// $('#totalpoints').html(profile.totalPoints);
	// for (var i = 0; i < campaign.length; i++) {
	// 	content+= '<li data-title="'+encodeURIComponent(campaign[i].name)+'" data-points="'+campaign[i].points+'" data-id="'+campaign[i].loyaltyID+'" class="'+((parseInt(profile.totalPoints) >= parseInt(campaign[i].points))? "redeem": "")+'">'+
	// 				'<div class="points"><div>'+campaign[i].points+'</div></div>'+
	// 				'<div class="desc">'+campaign[i].name+'</div>'+
	// 			  '</li>';
	// };
	// $('.campaign-list ul').html(content);

	e.preventDefault();
	nativeInterface.redeemWithNameAndPoints(e.target.getAttribute('data-id'), decodeURIComponent(e.target.getAttribute('data-title')), e.target.getAttribute('data-points')); 
	e.stopPropagation();
}

function redeemCallback(jsonString, title, points){

	var rewards = JSON.parse(jsonString);
	$("#home").removeClass('active-view');
	$("#campaign").addClass('active-view');
	
	// var rewards = [{"campaign": [{"loyaltyID": "LOYGE188151DlbRl","name": "Free Coffee","points": "10","frequency": "0","description": "<p>123</p>","terms": "testing","promoType": "snap"},{"loyaltyID": "LOYmr16955GILwhZ","name": "Cornetto 3 + 1","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App.","promoType": "frequency"},{"loyaltyID": "LOY5e151153dc8mf","name": "Aloha Burger","points": "10","frequency": "0","description": "<p>A big sarap burger</p>","terms": "secret","promoType": "snap"},{"loyaltyID": "LOYZE1612386j6yQ","name": "FREE Tumbler and 12oz Nescafe Shake","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>","terms": "","promoType": "frequency"},{"loyaltyID": "LOYQx161338TE43Y","name": "Sola 2 + 1","points": "2","frequency": "0","description": "<p>Test</p>","terms": "Test","promoType": "snap"}]},{"profile": {"raffleEntry": 0,"totalPoints": 12,"fname": "Bat","mname": "","lname": "Saver","email": "albert.madia@yahoo.com","mobileNum": "","landlineNum": "","dateReg": "2015-03-24","qrApp": "","memberID": "MEMYl121217GQZLC","redeemable_sku_promo": [{"loyaltyID": "LOYmr16955GILwhZ","count": 0,"group": "Selecta Cornetto"},{"loyaltyID": "LOYZE1612386j6yQ","count": 0,"group": "Nescafe Shake"}],"lastSync": "Oct 16,2015"}}]
	
	var profile = rewards[1].profile;
	var campaign = rewards[0].campaign;
	var content = " ";
 
	$('#user-email').html(profile.email);
	$('#totalpoints').html(profile.totalPoints);
	for (var i = 0; i < campaign.length; i++) {
		content+= '<li data-title="'+encodeURIComponent(campaign[i].name)+'" data-points="'+campaign[i].points+'" data-id="'+campaign[i].loyaltyID+'" class="'+((parseInt(profile.totalPoints) >= parseInt(campaign[i].points))? "redeem": "")+'">'+
					'<div class="points"><div data-label="'+((parseInt(campaign[i].points)==1) ? "POINT" : "POINTS")+'">'+campaign[i].points+'</div></div>'+
					'<div class="desc">'+campaign[i].name+'</div>'+
				  '</li>';
				  console.log((parseInt(profile.totalPoints) >= parseInt(campaign[i].points))? "redeem": "");
	};
	$('.campaign-list ul').html(content);

	// modalHide();

	// $(".prompt-modal ").show();
	// $('.prompt-modal .msg').html("You have successfully redeemed " + decodeURIComponent(title));
	// setTimeout(function(){
	// 	$("#success").hide();
	// }, 2000)

	$("#modal").removeClass('choice').addClass('prompt');
	$("#modal #prompt .image").addClass('active');
	$("#modal #prompt .msg").html("You have successfully redeemed " + decodeURIComponent(title));
	setTimeout(function(){
		modalHide();
	}, 2000)
}

function register(){	
	// $('.reg-modal').show();
	nativeInterface.registerCard();
}

function showRegForm(){ 
	$('.reg-modal').show(); 
}

// REGISTRATION
function validateRegistration(){
	var email = $("#email").val();
	var cmail = $("#confirm").val();

	$(".reg-modal input").blur();

	if (email == '') {
		$("#email").focus();
		nativeInterface.msgBox('Please enter your Email!', 'Empty Field!')
	}else if (!validateEmail(email)) {
		$("#email").focus();
		nativeInterface.msgBox('Email is invalid!','Invalid Input!')
	}else if (cmail == '') {
		$("#confirm").focus();
		nativeInterface.msgBox('Please confirm your Email!', 'Empty Field!')
	}else if (!validateEmail(cmail)) {
		$("#confirm").focus();
		nativeInterface.msgBox('Email is invalid!','Invalid Input!')
	}else if (email != cmail) {
		$("#email").focus();
		nativeInterface.msgBox('Emails does not match!','Invalid Input!')
	}else{
		nativeInterface.registerEmail(email);
	}
}

function validateEmail(email){
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function registrationCallback(){
	$(".reg-modal input").val('').blur();
	$(".reg-modal").hide();
}

function checkIn(){
	nativeInterface.checkIn();
}

function settings(){ 
	nativeInterface.settings(); 
}

function logout(){
	nativeInterface.done();
}

